# !pip install biopython

from Bio.PDB import PDBParser

class PDBreader:
    '''
    Serves as a design for a PDBreader object, which accepts a protein's PDB filename
    and outputs information about residues on each chain which have been sequenced,
    but whose coordinates cannot be identified (missing residues).

    '''
    def __init__(self, fname = ''):
        '''
        Accepts filename as input, if no input,
        reads from stdin (like FastAReader)
        '''
        if fname is '':
            self.fname = input('Enter your PDB filename: ')
        else:
            self.fname = fname

        self.chainDict = {} #keeps track of residues by chain
        self.chainComp = {}
        #keeps track of residues by class (polar, nonpolar, positive & negative charged)
        self.moleculeAndChain = {} #stores chain information by chain
        self.categoryFract = {} #stores the fraction that each class of amino acid makes up of each chain's missing residues
        self.printList = [['Molecule Name:','Chain:','Nonpolar:','Polar:','Positive:', 'Negative:']]
        #a masterlist of lists to be printed in final output

    def readPDB(self):
        '''
        Takes advantage of the Biopython module to parse out necessary information about
        missing residues and the chains they are located on
        '''
        structureID = self.fname.rstrip('.pdb') #structure ID is just the filename without the filename extention
        parser = PDBParser(PERMISSIVE=1)
        #parser object created with permissive tag, which is used to bypass common errors associated with some pdb files
        self.structure = parser.get_structure(structureID, self.fname) #structure object has access to header of PDB file
        self.missingRes = self.structure.header["missing_residues"]
        #returns a list of dictionaries, where each dict contains information about and corresponds with each missing residue
        #   ex: {'model': None, 'res_name': 'MET', 'chain': 'B', 'ssseq': 1, 'insertion': None}
        self.dictOfDict = self.structure.header['compound'] #dictionary with various information about the crystallized compound
        missingBool = self.structure.header["has_missing_residues"] #checks if the protein has missing residues
        if missingBool is True: #if missing reidues are present, calls the rest of the functions
            self.chainParser()
            self.resCounter()
            self.addList()
        else:
            print('No missing residues found.')
            #if no missing residues are found, the program terminates and prints the user a helpful message

    def chainParser(self):
        '''Sorts information parsed from readPDB into dictionaries by chain'''

        for dict in self.missingRes:
            chain = dict['chain'] #parses chain name from each residue's dict
            self.chainDict[chain] = [] #instantiates each chain as a key with empty list as value
            self.categoryFract[chain] = {'polar': 0, 'nonpolar': 0, 'negative': 0, 'positive': 0}
            #instantiates a key for each chain for categoryFract with a baseline dictionary as each chain's value
        for dict in self.missingRes:
            chain = dict['chain']
            self.chainDict[chain].append(dict) #adds each residue dictionary of chain to list value of chain key
            #final dictionary looks like: chainDict = {chain: [dict1, dict2, dict3]} where chain is the chain name
            #   & each dict is the dictionary of information corresponding to each residue
        for dict in self.dictOfDict.values(): #loops through the chains
            thisMolecule = dict['molecule']
            moleculeChain = dict['chain']
            self.moleculeAndChain.update({moleculeChain.upper() : thisMolecule})
            #used .upper() because the chain name is lowercase in these dicts
            #   & we need them to be consistent to search for them during formatting

    def resCounter(self):
        '''Counts the residues and their percentage composition of each chain'''

        for chain in self.chainDict: #the entire method loops through the chains in our chainDict
            chainCounts = 0 #total residues in the chain
            aaDict = {'polar': {'SER': 0, 'THR': 0, 'TYR': 0, 'ASN': 0, 'GLN': 0},
                      'nonpolar': {'GLY': 0, 'ALA': 0, 'VAL': 0, 'CYS': 0, 'PRO': 0, 'LEU': 0, 'ILE': 0, 'MET': 0, 'TRP': 0, 'PHE': 0},
                      'negative': {'ASP': 0, 'GLU': 0},
                      'positive': {'LYS': 0, 'ARG': 0, 'HIS': 0}}
            #this dictionary goes inside the for loop because we are counting the number of each residue per chain

            for dict in self.chainDict[chain]: #looping through each residue in the chain
                chainCounts += 1 #number of residues in each chain
                aa = dict['res_name']
                for category in aaDict:
                    if aa in aaDict[category].keys(): #checking if the residue is a part of each amino acid class
                        aaDict[category][aa] += 1 #if so, count it in aaDict
                        self.categoryFract[chain][category] += 1 #also count it in categoryFract
            for category in self.categoryFract[chain]:
                self.categoryFract[chain][category] = self.categoryFract[chain][category]/chainCounts
                #once we have the final counts, replace counts with fractions

            self.chainComp[chain] = aaDict
            #adds the entire dictionary with the percentages of each amino acid to the chain they correspond with

    def addList(self):
        '''Adds chain information and residue percentages to printList'''

        for eachDict in self.dictOfDict:
            oneDict = self.dictOfDict[eachDict]
            thisMolecule = oneDict['molecule']
            moleculeChain = oneDict['chain'].upper()
            non = str(round(self.categoryFract[moleculeChain]['nonpolar'] * 100, 2)) + '%'
            pol = str(round(self.categoryFract[moleculeChain]['polar'] * 100, 2)) + '%'
            pos = str(round(self.categoryFract[moleculeChain]['positive'] * 100, 2)) + '%'
            neg = str(round(self.categoryFract[moleculeChain]['negative'] * 100, 2)) + '%'
            #converting raw fractions to rounded percentages as strings for printing
            moleculesAndChains = [thisMolecule, moleculeChain, non, pol, pos, neg]
            #the order of this list is the order in which the components of
            #   each line (except the first) will be printed
            self.printList.append(moleculesAndChains) #add the line (list) to the masterlist of lines to be printed
        return self.printList

    def formatData(self):
        '''
        Formats and prints the lines of printList
        This method of formatting we found on Stack Overflow
        link: https://stackoverflow.com/questions/9989334/create-nice-column-output-in-python
        '''
        columnSpacing = max(len(item) for row in self.printList for item in row) + 2  # padding between each column
        for row in self.printList:
            print (''.join(item.ljust(columnSpacing) for item in row)) #aligns to the left
        for chain in self.chainComp:
            print('Chain: ' + chain + '\nResidue counts: ', end = " ")
            for category in self.chainComp[chain]:
                print("\n" + category + ':')
                for residue in self.chainComp[chain][category]:
                    print(residue, self.chainComp[chain][category][residue], end = ", ")
        #formatting is different for the residue counts because the sheer amount of text needed to be
        #   printed would not work for the formatting style of the rest of the data

def main():

    myReader = PDBreader('5ife.pdb')
    myReader.readPDB()
    myReader.formatData()

main()
